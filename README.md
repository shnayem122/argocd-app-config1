This is an argoCD deployment for practice. The deployment worked as expected and any changes to git repository will be implemented to the production using immediately. 

## Comands:
`
### Install ArgoCD in k8s
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

### Access ArgoCD UI
kubectl get svc -n argocd
kubectl port-forward svc/argocd-server 8080:443 -n argocd

### Login with admin user and below token (optional for private repo)
kubectl get secret argocd-initial-admin-secret -n argocd -o yaml
echo {secretPass} | base64 -d

### Start Deployment
kubectl apply -f application.yaml`

<h3>Argo CD Deployment</h3>
![argo](./assets/argo.png)
